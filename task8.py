from datetime import datetime
import argparse
import numpy as np


def parser_func():  # Argument Praising
    parser = argparse.ArgumentParser(description="Input Path")
    parser.add_argument(
        '-path', help='Used to give path to text file for letency')
    return parser.parse_args()


def read_file(path):
    data = []
    with open(path, 'r') as file:
        for num in file:
            data.append(int(num)/1000)
    return np.array(data)


def main():
    args = parser_func()
    print("\n--------------------------\n")
    print(f"Date-Time:  {datetime.now()}")
    print("Stats for Websocket Latency")
    print("\n--------------------------\n")

    data = read_file(args.path)
    print(f"Mean: {np.mean(data):.2f}")
    print(f"Median: {np.median(data):.2f}")
    print(f"Min: {np.min(data):.2f}")
    print(f"Max: {np.max(data):.2f}")
    print(f"Standard Deviation: {np.std(data):.2f}")
    print(f"90th percentile: {np.percentile(data,90):.2f}")
    print(f"99th percentile: {np.percentile(data,99):.2f}")
    print(f"99.9th percentile: {np.percentile(data,99.9):.2f}")
    print(f"99.99th percentile: {np.percentile(data,99.99):.2f}")
    print(f"99.999th percentile: {np.percentile(data,99.999):.2f}")
    print(f"Total values: {np.size(data):.2f}\n")

    return


main()
