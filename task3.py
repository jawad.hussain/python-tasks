# check shebang
#!/usr/bin/env python3
from cpuinfo.cpuinfo import get_cpu_info, os, sys
import cpuinfo
import psutil


info = cpuinfo.DataSource.lscpu()
info = info[1].split('\n')
word1 = info[8].split()
word2 = info[19].split()

path = os.path.expanduser('~')
path = os.path.join(path, "Details")

if not os.path.exists(path):
    os.mkdir(path)

path = os.path.join(path, "Summary.txt")

data = get_cpu_info()
cpufreq = psutil.cpu_freq()

file = open(path, "w")

file.write("Byte Order: ")
file.write(f"{sys.byteorder} Endian")

file.write("\nCore(s) per socket: ")
file.write(str(psutil.cpu_count()))

file.write("\nSocket(s) :")
file.write(word1[-1])

file.write("\nModel name: ",)
file.write(data["brand_raw"])

file.write("\nCPU MHz: ")
file.write(str(f"{cpufreq.current:.2f} Mhz"))

file.write("\nCPU max MHz: ")
file.write(str(f"{cpufreq.max:.2f} Mhz"))

file.write("\nCPU min MHz: ")
file.write(str(f"{cpufreq.min:.2f} Mhz"))

file.write("\nVirtualization Support: ")
file.write(word2[-1])

file.write("\nL1 cache: ")
file.write(f"{data['l1_data_cache_size']}")

file.write("\nL2 cache: ")
file.write(f"{data['l2_cache_size']}")

file.write("\nL3 cache: ")
file.write(f"{round(data['l3_cache_size']/1000)}KB")

file.write("\nRAM Memory: ")
file.write(f"{round(psutil.virtual_memory().total/1000000, 2)} MB")

file.close()

# try using lscpu with grep
