import random
import argparse
import json


def parser_func():  # Argument Praising
    parser = argparse.ArgumentParser(description="Input iterations")
    parser.add_argument('-i', type=int, metavar='',
                        help='Input the number of iterations. i.e: -i <value>')
    parser.add_argument('-j', action='store_true',
                        help='Take value from iteration.json file ')
    return parser.parse_args()


def arg_decision(args):
    if args.j == False:
        interval = args.i
    else:
        with open("iteration.json", 'r') as file:
            itr = file.read()
            itr = json.loads(itr)[0]
            interval = itr['iteration']
    return interval


def carlos_sim_calc(interval):  # Carlo's Simulation calculator
    circle_points = 0
    square_points = 0
    for i in range(interval):

        rand_x = random.uniform(-1, 1)
        rand_y = random.uniform(-1, 1)

        origin_dist = rand_x**2 + rand_y**2

        if origin_dist <= 1:
            circle_points += 1

        square_points += 1
    return 4 * circle_points / square_points


def main():
    print("\n---------- Task 4 ----------\n")
    args = parser_func()
    if args.i is None and args.j == False:
        print("\nNo Argument Detected. Program will be terminated. \n\nPlease Enter either -i <value> or -j.\n\nType -h to see what these arguments do.\n")
        return
    interval = arg_decision(args)
    pi = carlos_sim_calc(interval)
    print("Final Estimation of Pi=", pi)
    print("\n\n----------- Done -----------\n")


main()
