import asyncio
import websockets
import json
import time
import logging
import argparse
from os import system, name
from datetime import datetime


def clear():

    # for windows
    if name == 'nt':
        _ = system('cls')

    # for mac and linux(here, os.name is 'posix')
    else:
        _ = system('clear')


logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


def parser_func():  # Argument Praising
    parser = argparse.ArgumentParser(description="Input iterations")
    parser.add_argument(
        '-path', help='Used to give path to json file for messeges')
    parser.add_argument('-C', action='store_false',
                        help='Disable console logging')
    parser.add_argument('-F', action='store_true',
                        help='Enable file logging')
    return parser.parse_args()


def create_console_log(logger):
    stream_handler = logging.StreamHandler()
    stream_handler.setLevel(logging.INFO)
    logger.addHandler(stream_handler)


def create_file_log(logger):

    file_handler = logging.FileHandler('client.log')
    file_handler.setLevel(logging.DEBUG)

    logger.addHandler(file_handler)


async def client(msgs):
    uri = "ws://localhost:8765"
    async with websockets.connect(uri) as websocket:
        logger.info(
            "\n==========================================================\n")
        logger.info(f"INFO: Connection Established at {datetime.now()}\n")
        for msg in msgs:

            logger.info(
                "\n-----------------------------------------------------------\n")

            msg['tx_time'] = time.time_ns()
            msg = json.dumps(msg)
            await websocket.send(msg)
            logger.info(f"INFO: >> Message Sent: {json.loads(msg)}\n")
            msg = await websocket.recv()
            msg = json.loads(msg)
            logger.info(f"INFO: << Message Recieved: {msg}\n")
    logger.info(
        "\n-----------------------------------------------------------\n\n")
    logger.info("INFO: Connection Closed")


def read_file(path):
    logger.debug(f"\nDEBUG: Data read from file {path}\n")
    with open(path, 'r') as file:
        return json.load(file)


def arg_decision(args):

    if(bool(args.F) == True):
        create_file_log(logger)
        print("Creating File Log")
        logger.info(
            "\n\n==========================================================\n")
    logger.debug("\nDEBUG: file_Logging Enabled")
    if(bool(args.C) == True):
        create_console_log(logger)
        print("Creating Console Log")
        logger.debug("\nDEBUG: Console_Logging Enabled")


if __name__ == "__main__":

    clear()

    args = parser_func()

    arg_decision(args)

    msgs = read_file(args.path)

    asyncio.run(client(msgs))
