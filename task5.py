#   NOTE: The veriable name should not be list as it is also a command.
# But as it was requirment of the task hence list was used.

import matplotlib.pyplot as plt


def square(list):
    square = [numbers ** 2 for numbers in list]
    plt.plot(list, square)
    plt.xlabel("Input List")
    plt.ylabel("Square of List")
    plt.title(label="List vs Square of List", fontsize=20, color="green")
    plt.show()
    return square


def cube(list):
    cube = [numbers ** 3 for numbers in list]
    plt.plot(list, cube)
    plt.xlabel("Input List")
    plt.ylabel("Cube of List")
    plt.title(label="List vs Cube of List ", fontsize=20, color="green")
    plt.show()
    return cube


def main():
    print("\n---------- Task 5 ----------\n")
    x = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    print("input Values: ", x)
    print("Square: ", square(x))
    print("Cube: ", cube(x))
    return
    print("\n\n----------- Done -----------\n")


main()
