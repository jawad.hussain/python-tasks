# Task 9

def main():
    print("\n---------- Task 9 ----------\n")

    num = int(input("Enter number of values to entered: "))
    list = []
    count_dict = {}
    for iter in range(num):
        word = input()
        list.append(word)
        if word in count_dict:
            count_dict[word] += 1
        else:
            count_dict[word] = 1

    print("\n------------------")
    print("Desired Output:")
    print("------------------")

    print(len(count_dict))
    for word in count_dict:
        print(count_dict[word], end=" ")

    print("\n\n------------------")
    print("Descriptive Output:")
    print("------------------")

    print("\nTotal Distinct words: ", len(count_dict))
    for k, v in count_dict.items():
        print(f"Occurance of the word {k} is {v}")

    print("\n\n----------- Done -----------\n")


main()
