# Python Tasks:

## Version and Required Libraries/ Modules:
- Python version: 3.9.7 *( It can run on python 3 and above )*.
- Libraries:  
  1. argparse
  2. psutil
  3. cpuinfo
  4. json
  5. random
  6. matplotlib
  7. logging
  8. asyncio
  9. pprint
  10. datetime  
- **NOTE:** Most of these Libraries are already provided with python 3.7 and above.
## How to Run:
**Setup:**
- Clone repository using command `git clone https://gitlab.com/jawad.hussain/python-tasks.git`
- **NOTE:** 
  - Make sure to begin every task from start of cloned repository.
  - Make sure all above mantioned libraries are installed.

1. **Task 1:**  
   - Run Command `python3 task1.py` 
2. **Task 2:**
   - Run Command `Python3 task2.py <flag1> <argument1> <flag2> <argument2> <flag3> <argument3> <flag4> <argument4>`
   - **NOTE:** Value of flags has to be either `-UP, -DOWN, -LEFT or -RIGHT`. They can be in any order but must be used only once.
   - If an argument and its flag is not given the value of that flag will default to zero.
   - Both argument and corsponding flag must be given togather or emitted altogater. 
3. **Task 3:**
   - Run Command `python3 task3.py`
4. **Task 4:**
   - Go to directory task4. command: `cd task4`
   - Run Command `python3 task4.py <flag1> <argument>` or `python3 task4.py <flag2>`
   - **flag = -i** : To input value of iterator
   - **argument** : Iterator value for flag1
   - **flag = -j** : To get value of iterator form json file
  
5. **Task 5:**
   - Run Command `python3 task5.py`
6. **Task 6:**
   - Go to directory task6. command: `cd task6`
   - Run Command `python3 find_divisibles.py` and `python3 find_divisibles_async.py`
7. **Task 7:**
   - Go to directory task7. command: `cd task7` 
   - Run Command `python3 server.py <flag1> <flag2>` and then `python3 client.py <flag1> <argument> <flag2> <flag3>`
   - *server flags*:
     - **-F** = create log file
     - **-C** = dont log no console 
   - *client flags:*
     - **flag1** = -path ; **argument** = path if message.json file
     - **-F** = create log file
     - **-C** = dont log no console 
     - flags can be in any order as long as correct argument is with correct flag
   - **NOTE:** server must be run before client. Also server must be closed by intrupt i.e `CTRL + c`
8. **Task 8:**
   - Run Command `python3 task8.py <flag1> <argument>`
   - *flags:*
     - **flag1** = -path ; **argument** = path of .txt file with data 
9.  **Task 9:**
    -Run Command `python3 task9.py` 

## Description:

1. **Task 1:**    
   Program findsnumbers divisible by 7 but not multiple of 5. Numbers are from 2000 to 3200. 
2. **Task 2:**   
   User gives different steps in up, down, left, right direction and the program determains the final position of user from origin.
3. **Task 3:**    
   Output System Details in Summery.txt in home/user/Details directory.
4. **Task 4:**  
   Impilimenting Monte Carlo's simulation for finding value of "pi". The number of iterations can be given from user or taken from json file. This is decided by command line arguments.
  
5. **Task 5:**  
   Program takes square and cube of a list and then plots and dispalys its graph respectively.
6. **Task 6:**  
   `find_divisibles.py` finds numbers divisble by a specific number in a specific range. Both range and divisor are hard coded. This is done with 3 seperate sets of numbers and the whole process us timed. In the end the all the data is logged in log file.  
   `find_divisibles_async.py` does the same this but with the addition of running the three sets asynchronously. In the end the data is logged in log flie.
7. **Task 7:**  
   These programs i.e: `client.py` and `server.py` create a acynchronous socket connection where client transfers 100000 messages ( or how many ever there are in the json file) from json file to server with time stamp. The server adds its own time stamp and calculates latency. Then server send all this data back to client. Both client and server Log the data where the user enable/disables console and file logging. The user us also suppose to provide path to message.json to client.
8. **Task 8:**  
   This Programs uses the latency file generated from the previous task to calculate different expressions such as mean, median, standard daviation.
9. **Task 9:**  
    Program asks user for a few strings. number of strings is decided by the user. It afterwards displays the amount of unique strings and the amount of occurance of every string.