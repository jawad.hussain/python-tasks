# TASK: Please write a program to compute the distance from the current position after a sequence of movement
# and original point. If the distance is a float, then just print the nearest integer. Use argparse library to
# take inputs for UP, DOWN, LEFT and RIGHT. Use of functions is encouraged.


import argparse
import math

# Argument Praising
parser = argparse.ArgumentParser(description="Input Directions")
parser.add_argument('-UP', type=float, metavar='',
                    default=0, help='Steps moved Upwards')
parser.add_argument('-DOWN', type=float, metavar='', default=0,
                    help='Steps moved Downwards')
parser.add_argument('-LEFT', type=float, metavar='', default=0,
                    help='Steps moved towards Left')
parser.add_argument('-RIGHT', type=float, metavar='', default=0,
                    help='Steps moved towards Right')
args = parser.parse_args()


# Function Definations
def y_axis():
    return args.UP - args.DOWN


def x_axis():
    return args.RIGHT - args.LEFT


def compute_loc(x, y):
    if y < 0:
        print("User is %s step(s) Downward from origin" % (abs(y)))
    elif y > 0:
        print("User is %s step(s) Upward from origin" % (y))
    elif y == 0 and x != 0:
        print("User is 0 steps Upward or Downward from origin")

    if x < 0:
        print("User is %s step(s) Left from origin" % (abs(x)))
    elif x > 0:
        print("User is %s step(s) Right from origin" % (x))
    elif y != 0 and x == 0:
        print("User is 0 steps Left or Right from origin")

    if x == 0 and y == 0:
        print("User is at Origin")


# Main
def main():
    print("\n---------- Task 2 ----------\n")
    y = y_axis()
    x = x_axis()
    compute_loc(x, y)
    print(f"Distance: {round(math.sqrt(x**2 + y**2))}")
    print("\n\n----------- Done -----------\n")


main()
