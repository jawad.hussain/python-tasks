import logging
import time
import asyncio
from pprint import pprint

logging.basicConfig(filename="task6_async.log",
                    format='%(asctime)s %(message)s', filemode='w')
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)


async def find_divisibles(in_range, divisor):
    logger.debug("find_divisibles called with range %s and divisor %s." %
                 (in_range, divisor))

    start_time = time.time()
    list = []

    for num in range(1, in_range+1):
        if num % divisor == 0:
            list.append(num)
            await asyncio.sleep(0)
    end_time = time.time()
    logger.debug("find_divisibles called with range %s and divisor %s. It took %s seconds" %
                 (in_range, divisor, end_time - start_time))

    return list


async def main():
    print("\n---------- Task 6 ----------\n")
    start_time = time.time()

    task1 = asyncio.create_task(find_divisibles(50800000, 34113))
    task2 = asyncio.create_task(find_divisibles(100052, 3210))
    task3 = asyncio.create_task(find_divisibles(500, 3))
    await asyncio.wait([task1, task2, task3])

    print("\nValues for (50800000, 34114):\n")
    pprint(task1.result())

    print("\nValues for (100052, 3210):\n")
    pprint(task2.result())

    print("\nValues for (500, 3):\n")
    pprint(task3.result())

    end_time = time.time()
    print("\n\nTotal time consumed: ", end_time - start_time)

    print("\n\n----------- Done -----------\n")


asyncio.run(main())
