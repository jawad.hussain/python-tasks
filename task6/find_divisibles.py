import logging
import time
from pprint import pprint

logging.basicConfig(filename="task6.log",
                    format='%(asctime)s %(message)s', filemode='w')
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)


def find_divisibles(in_range, divisor):
    logger.debug("find_divisibles called with range %s and divisor %s." %
                 (in_range, divisor))

    start_time = time.time()
    list = []

    for num in range(1, in_range+1):
        if num % divisor == 0:
            list.append(num)
    end_time = time.time()
    logger.debug("find_divisibles called with range %s and divisor %s. It took %s seconds" %
                 (in_range, divisor, end_time - start_time))

    return list


def main():
    print("\n---------- Task 6 ----------\n")
    start_time = time.time()

    print("\nValues for (50800000, 34114):\n")
    pprint(find_divisibles(50800000, 34113))

    print("\nValues for (100052, 3210):\n")
    pprint(find_divisibles(100052, 3210))

    print("\nValues for (500, 300):\n")
    pprint(find_divisibles(500, 3))

    end_time = time.time()
    print("\n\nTotal time consumed: ", end_time - start_time)

    print("\n\n----------- Done -----------\n")


main()
